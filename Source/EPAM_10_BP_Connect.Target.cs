// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class EPAM_10_BP_ConnectTarget : TargetRules
{
	public EPAM_10_BP_ConnectTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "EPAM_10_BP_Connect" } );
	}
}
