// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class EPAM_10_BP_ConnectEditorTarget : TargetRules
{
	public EPAM_10_BP_ConnectEditorTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "EPAM_10_BP_Connect" } );
	}
}
