// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EPAM_10_BP_ConnectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class EPAM_10_BP_CONNECT_API AEPAM_10_BP_ConnectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
