// Copyright Epic Games, Inc. All Rights Reserved.

#include "FileWriterBPLibrary.h"
#include "FileWriter.h"

UFileWriterBPLibrary::UFileWriterBPLibrary(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{

}

bool UFileWriterBPLibrary::WriteToFile(FString text, FString filename, bool isAppend)
{
	FString file = FPaths::ProjectDir();
	file.Append(filename);

	return FFileHelper::SaveStringToFile(text, *file,
		FFileHelper::EEncodingOptions::AutoDetect,
		&IFileManager::Get(),
		isAppend ? EFileWrite::FILEWRITE_Append : EFileWrite::FILEWRITE_None);
}

