// Copyright Epic Games, Inc. All Rights Reserved.

#include "CalculatorPluginBPLibrary.h"
#include "CalculatorPlugin.h"

UCalculatorPluginBPLibrary::UCalculatorPluginBPLibrary(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer) {}

FString UCalculatorPluginBPLibrary::AppendSymbol(FString text, FString symbol)
{
	TSet<char> allowedOps{ '+', '-', '*', '/' };
	TSet<char> allowedSymbols{ '.' };

	if (symbol.Len() != 1) return FString();
	if (!(allowedSymbols.Contains(symbol[0]) ||
		allowedOps.Contains(symbol[0]) ||
		(symbol[0] >= '0' && symbol[0] <= '9'))) return FString();

	if (text.Len() == 1 && text[0] == TEXT('0') && (symbol[0] >= '0' && symbol[0] <= '9'))
	{
		text = symbol;
	}
	else if (text.Len() > 0 && text[text.Len() - 1] == TEXT('.') && !(symbol[0] >= '0' && symbol[0] <= '9'))
	{
		text.AppendChar(TEXT('0'));
		text.Append(symbol);
	}
	else
	{
		text.Append(symbol);
	}

	if (text.EndsWith(TEXT(".0"))) text = text.LeftChop(2);
	
	return text;
}

FString UCalculatorPluginBPLibrary::RemSymbol(FString text)
{
	if (!text.IsEmpty()) text.RemoveAt(text.Len() - 1);

	if (text.IsEmpty()) text = TEXT("0");

	if (text.EndsWith(TEXT(".0"))) text = text.LeftChop(2);

	return text;
}

FString UCalculatorPluginBPLibrary::Calculate(FString text)
{
	float result = Calc(text);
	text = FString::SanitizeFloat(result);
	if (text.EndsWith(TEXT(".0"))) text = text.LeftChop(2);

	return text;
}

float UCalculatorPluginBPLibrary::Calc(FString text)
{
	FString lhs, rhs;
	char op;
	bool status = SplitByOps(text, lhs, rhs, op);

	if (!status) return FCString::Atof(*text);

	switch (op)
	{
	case '*': return Calc(lhs) * Calc(rhs);
	case '/':
	{
		float d = Calc(rhs);
		if (d == 0)
		{
			UE_LOG(LogTemp, Error, TEXT("Division by 0"));
			return 0;
		}
		return Calc(lhs) / d;
	}
	case '+': return Calc(lhs) + Calc(rhs);
	case '-': return Calc(lhs) - Calc(rhs);
	default:
		UE_LOG(LogTemp, Error, TEXT("Unsupported op %c"), op);
	}

	return 0;
}

bool UCalculatorPluginBPLibrary::SplitByOps(FString src, FString& lhs, FString& rhs, char& op)
{
	int mulPos = INDEX_NONE;
	int divPos = INDEX_NONE;
	int sumPos = INDEX_NONE;
	int subPos = INDEX_NONE;

	src.FindLastChar(TEXT('*'), mulPos);
	src.FindLastChar(TEXT('/'), divPos);
	src.FindLastChar(TEXT('+'), sumPos);
	src.FindLastChar(TEXT('-'), subPos);

	// order of ops must be reverse because of recursive call

	if (sumPos != INDEX_NONE && ((subPos != INDEX_NONE && sumPos > subPos) || subPos == INDEX_NONE))
	{
		op = '+';
		SplitByIndex(src, lhs, rhs, sumPos);
		return true;
	}

	if (subPos != INDEX_NONE && ((sumPos != INDEX_NONE && subPos > sumPos) || sumPos == INDEX_NONE))
	{
		op = '-';
		SplitByIndex(src, lhs, rhs, subPos);
		return true;
	}

	if (mulPos != INDEX_NONE && ((divPos != INDEX_NONE && mulPos > divPos) || divPos == INDEX_NONE))
	{
		op = '*';
		SplitByIndex(src, lhs, rhs, mulPos);
		return true;
	}

	if (divPos != INDEX_NONE && ((mulPos != INDEX_NONE && divPos > mulPos) || mulPos == INDEX_NONE))
	{
		op = '/';
		SplitByIndex(src, lhs, rhs, divPos);
		return true;
	}

	return false;
}

void UCalculatorPluginBPLibrary::SplitByIndex(FString src, FString& lhs, FString& rhs, int index)
{
	if (index == 0) lhs = TEXT("");
	else lhs = src.LeftChop(src.Len() - index);

	if (index == src.Len() - 1) rhs = TEXT("");
	else rhs = src.RightChop(index + 1);
}