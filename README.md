# EPAM_10_BP_Connect

Developed with Unreal Engine 5

Write a custom UE4 plugin which will add custom methods for Blueprints. (Using C++, C#, Editor, Blueprints).  

For example: 

1 - Make a simple menu: with Blueprints (input field and a button) and with C++ (button triggers entered to be written into a file) 

2 – Make a simple calculator (+,-,*,/) UI - in  Blueprints; – calculations - in С++. 

https://docs.unrealengine.com/4.27/en-US/ProgrammingAndScripting/ClassCreation/CodeAndBlueprints/ 

https://www.youtube.com/watch?v=cJICy_o5ljs 

![1 Level BP.](/readme/1_Lvl_BP.PNG "1 Level BP.")

![1 UI.](/readme/1_UI.PNG "1 UI.")

![1 UI BP.](/readme/1_UI_BP.PNG "1 UI BP.")

![2 Level BP.](/readme/2_Lvl_BP.PNG "2 Level BP.")

![2 UI.](/readme/2_UI.PNG "2 UI.")

![2 UI BP.](/readme/2_UI_BP.PNG "2 UI BP.")

Demo: https://youtu.be/xE_cxcDSSeA